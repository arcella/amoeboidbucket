using UnityEngine;
using System.Collections;

public class NewBehaviourScript : MonoBehaviour 
{
	public Transform clickObject;
	GameObject spawnPos;

	// Use this for initialization
	void Start () 
	{
		spawnPos = GameObject.FindGameObjectWithTag("boxes");
	}
	
	// Update is called once per frame
	void Update () 
	{
		Click();
	}
	
	
	public void Click()
	{
		if ( Input.GetMouseButtonDown (0))
		{
			Instantiate(clickObject, spawnPos.transform.position, Quaternion.identity);
			
		}
	}
	
	
}
