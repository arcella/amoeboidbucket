using UnityEngine;
using System.Collections;

public class TouchInput : MonoBehaviour
{
	
	public Vector3 mTouchPos;
	public GameObject mAmoeba;
	public float pushForce = 100;
	
	// UI
	public GameObject mTouchPosIndicator;
	
	// Use this for initialization
	void Start ()
	{
		
	}

	
	// Update is called once per frame
	void Update ()
	{
		//Vector3 pos = ScreenPositionToWorld(Input.GetTouch(0).position); // TOUCH
		Vector3 pos = ScreenPositionToWorld(Input.mousePosition); // MOUSE
		
		mouseClick(pos);
		
		//TouchManager();
	}
	
	// :::::::::::::::::::::::::::::::::::::::::::::: Methods
	public void TouchManager()
	{
		if(Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Moved)
		{
			Vector2 touchDeltaPos = Input.GetTouch(0).deltaPosition;
			mAmoeba.transform.Translate (-touchDeltaPos.x, -touchDeltaPos.y, 0);
		}
	}
	
	// ::::::::::: Mouse Input for testing purposes
	// To be changed to touch
	public void mouseClick (Vector3 forcePos)
	{

		
		if (Input.GetMouseButtonDown(1))
		{
			mTouchPosIndicator.transform.position = ScreenPositionToWorld(Input.mousePosition);
		}
	}
	
	// ::::::::::: Screen position to world
	Vector3 ScreenPositionToWorld(Vector3 ScreenPos)
	{
		Ray rayLoc = camera.ScreenPointToRay(ScreenPos);
		Plane xyPlane = new Plane(new Vector3(0.0f, 0.0f, 1.0f), new Vector3(0.0f, 0.0f, 0.0f));
		float camDistToLoc = 0.0f;
		xyPlane.Raycast(rayLoc, out camDistToLoc);
		Vector3 mousePlaneLoc = rayLoc.GetPoint(camDistToLoc);
		return mousePlaneLoc;
	}
	
	
}
