using UnityEngine;
using System.Collections;

public class LiftDragObject : MonoBehaviour 
{
	public Vector3 mMoveLimits;
	public float heightLimit;
	
	Vector3 startPos;
	Vector3 currPos;
	
	
	// Use this for initialization
	void Start () 
	{
		startPos = transform.position;
		mMoveLimits.x = transform.position.x;
		mMoveLimits.y = transform.position.y + heightLimit;
	}
	
	// Update is called once per frame
	void Update () 
	{
		currPos = this.transform.position;
		
		//Constrain in X
		if(currPos.x != mMoveLimits.x)
		{
			Vector3 temp = currPos;
			temp.x = mMoveLimits.x;
			transform.position = temp;
		}
		
		// constrain in y
		if(currPos.y >= mMoveLimits.y)
		{
			Vector3 temp = currPos;
			temp.y = mMoveLimits.y;
			transform.position = temp;
		}
		
		
	}
}
