using UnityEngine;
using System.Collections;

public class AmoeboidBase : MonoBehaviour
{

	// Use this for initialization
	void Start ()
	{
	
	}
	
	// Update is called once per frame
	void Update ()
	{
	
	}
	
	// Convert from screen-space coordinates to world-space coordinates on the Z = 0 plane
	public static Vector3 GetWorldPos (Vector2 screenPos)
	{
		Ray ray = Camera.main.ScreenPointToRay (screenPos);

		// we solve for intersection with z = 0 plane
		float t = -ray.origin.z / ray.direction.z;

		return ray.GetPoint (t);
	}
}
