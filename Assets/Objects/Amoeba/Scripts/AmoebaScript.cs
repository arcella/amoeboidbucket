using UnityEngine;
using System.Collections;

public class AmoebaScript : AmoeboidBase
{
	// ::::::::::::: FSM
	public enum AmoebaStates
	{
		idle,
		moving,
		grabbing,
		squishing
	}
	
	
	public bool isFirstTimeState = false;
	public AmoebaStates currentState = AmoebaStates.idle;
	
	// ::::::::::::: Abilities
	
	// Edge Grab
	public GameObject grabRadGeom;
	public GameObject grabTarget;
	public float maxStretchDist = 2.5f;
	public Transform grabDest;
	bool isLedgeGrabbing = false;
	
	// Squish
	//public Vector3 squishScale = new Vector3(1,0.5f, 1); WORK ON THIS ONE!!!!!!!!!!!!!!!!!!!!!!
	
	
	// ::::::::::::: MOVEMENT
	public bool IsGrounded;
	public Vector3 mMoveDirection = new Vector3 (0, 0, 0);
	public Vector3 mMoveTarget;
	public Transform mTargetPrefab;
	public float mSpeed = 5;
	
	// ::::::::::::: Juicy-ness
	
	// line renderer
	public Color c1 = Color.yellow;
	public Color c2 = Color.red;
	
	

	// Use this for initialization
	void Start ()
	{
		isFirstTimeState = true;
		setupLineRenderer ();
	}
	
	// Update is called once per frame
	void FixedUpdate ()
	{
		StateMachine ();
		Pseudopod ();
		renderLine ();
	}
	
	
	
	// ::::::::::::: FSM
	void StateMachine ()
	{
		//Debug.Log (currentState);
		if (currentState != AmoebaStates.grabbing) {
			grabRadGeom.renderer.enabled = false;
		}
		
		switch (currentState) {
		case AmoebaStates.idle:
			//Debug.Log(currentState);
			// amoeba is idle
			
			break;
			
		case AmoebaStates.moving:
			//Debug.Log(currentState);
			// amoeba is moving
			
			AmoebaMove ();
			
			
			float dist = Vector3.Distance (mMoveTarget, transform.position);
			
			if (dist <= .5) {
				mMoveTarget = Vector3.zero;
				rigidbody.velocity = Vector3.zero;
				currentState = AmoebaStates.idle;
				Debug.Log ("reset dest");
			}
			
			
			break;
			
		case AmoebaStates.grabbing:
			grabRadGeom.renderer.enabled = true;
			
			
			break;
			
		case AmoebaStates.squishing:
			//transform.localScale.y = squishScale.y;
			
			break;
			
		}
	}
	
	
	// :::::::::::::::::::::::::::::: 
	// Touch Input
	// ::::::::::::::::::::::::::::::
	
	void OnDoubleTap (TapGesture gesture)
	{ 
		mMoveTarget = AmoeboidBase.GetWorldPos (gesture.Position);
		//Debug.Log("World Space is " + mMoveTarget);
		currentState = AmoebaStates.moving;
	}
	

	// :::::::::::::::::::::::::::::: 
	// METHODS
	// ::::::::::::::::::::::::::::::
	
	// <><><><><><><><><><><><><><><><><><><><> Movement
	void AmoebaMove ()
	{
		// Used for grabbing ledges
		if ((isLedgeGrabbing == true) && grabDest) {
			mMoveTarget = grabDest.position;
			mMoveTarget.y += 2.0f;
			mMoveDirection = (mMoveTarget - transform.position).normalized * mSpeed;
			rigidbody.velocity = mMoveDirection;
			
			float dist = Vector3.Distance (mMoveTarget, this.transform.position);
			
			if( dist <= .5)
			{
				grabDest = null;
				isLedgeGrabbing = false;
				return;
			}
		} 
		
		else if ((isLedgeGrabbing == false) && !grabDest) {
			mMoveTarget.y = transform.position.y;
			mMoveDirection = (mMoveTarget - transform.position).normalized * mSpeed;
			rigidbody.velocity = mMoveDirection;
			
			
			// CLEAR if not grounded
			if (!IsGrounded) {
				currentState = AmoebaStates.idle;
			}	
		}
	}
	
	void Pseudopod ()
	{
		Vector3 stretchVect = (grabTarget.transform.position - this.transform.position);
		float length = stretchVect.magnitude;
		
		if (length > maxStretchDist) {
			stretchVect.Normalize ();
			grabTarget.transform.localPosition = stretchVect * (maxStretchDist);
		}
	}
	
	public void setGrabDest ()
	{
		Debug.Log ("Destination of grab set");
		currentState = AmoebaStates.moving;
		isLedgeGrabbing = true;
		Debug.Log (grabDest.position);
	}
	

	// <><><><><><><><><><><><><><><><><><><><> Juicy-ness
	void setupLineRenderer ()
	{
		LineRenderer lineRenderer = gameObject.GetComponent<LineRenderer> ();
		//lineRenderer.material = new Material(Shader.Find("Particles/Additive"));
		lineRenderer.SetColors (c1, c2);
		//lineRenderer.SetWidth(0.2F, 0.2F);
	}
	
	void renderLine ()
	{
		LineRenderer lineRenderer = GetComponent<LineRenderer> ();
		lineRenderer.SetPosition (0, this.transform.position);
		lineRenderer.SetPosition (1, grabTarget.transform.position);

	}
	

	
	// ::::::::::::: Grounded
	void OnCollisionStay (Collision col)
	{
		IsGrounded = true;
	}
 
	void OnCollisionExit (Collision col)
	{
		IsGrounded = false;
	}
	
	
	
	
}
