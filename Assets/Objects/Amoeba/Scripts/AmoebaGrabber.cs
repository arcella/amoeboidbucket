using UnityEngine;
using System.Collections;

public class AmoebaGrabber : MonoBehaviour
{
	
	int fingerCount = 0;
	public AmoebaScript amoebaScript;
	
	
	public enum GrabberStates
	{
		idle,
		grabbing
	}
	
	private GrabberStates currentState = GrabberStates.idle;
	
	
	// Use this for initialization
	void Start ()
	{
		amoebaScript = GameObject.FindGameObjectWithTag("Player").GetComponent<AmoebaScript>();
	}
	
	// Update is called once per frame
	void Update ()
	{
		StateMachine();
	}
	
	
	void StateMachine ()
	{
		switch (currentState)
		{
			
		case GrabberStates.idle:
			
			//amoebaScript.currentState = AmoebaScript.AmoebaStates.idle;	
			this.transform.localPosition = Vector3.zero;
			
			break;
			
			
		case GrabberStates.grabbing:
			
			
			break;
		}
		
	}
	
	void OnGrabberDrag (DragGesture drag)
	{ 
		currentState = GrabberStates.grabbing;
	}
	
	void OnFingerUp (FingerUpEvent e)
	{
		currentState = GrabberStates.idle;
		amoebaScript.currentState = AmoebaScript.AmoebaStates.idle;
		this.transform.localPosition = Vector3.zero;
	}
	

	
	void OnTriggerEnter (Collider other)
	{
		if (other.gameObject.tag == "edge") {
			amoebaScript.grabDest = other.gameObject.transform;
			amoebaScript.setGrabDest();
		}
	}
}
